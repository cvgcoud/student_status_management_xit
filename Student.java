package com.system.po;

import java.util.Date;

public class Student {
    private Integer userid;

    private String username;

    private Integer userage;

    private String sex;

    private String birthyear;

    private String grade;

    private Integer collegeid;

    private String usernation;

    private String usersystem;

    private String userreward;

    private String userstudy;


    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Integer getUserage() {return userage;}

    public void setUserage(Integer userage) { this.userage = userage;}

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getBirthyear() {
        return birthyear;
    }

    public void setBirthyear(String birthyear) {
        this.birthyear = birthyear;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getCollegeid() {
        return collegeid;
    }

    public void setCollegeid(Integer collegeid) {
        this.collegeid = collegeid;
    }

    public String getUsernation() {
        return usernation;
    }

    public void setUsernation(String usernation) {
        this.usernation = usernation;
    }

    public String getUsersystem() {
        return usersystem;
    }

    public void setUsersystem(String usersystem) {
        this.usersystem = usersystem;
    }

    public String getUserreward() {
        return userreward;
    }

    public void setUserreward(String userreward) {
        this.userreward = userreward;
    }

    public String getUserstudy() {
        return userstudy;
    }

    public void setUserstudy(String userstudy) {
        this.userstudy = userstudy;
    }
}